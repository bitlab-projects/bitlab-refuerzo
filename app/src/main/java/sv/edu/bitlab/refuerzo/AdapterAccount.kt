package sv.edu.bitlab.refuerzo

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView

class AdapterAccount(private var accountList: MutableList<Account>?, private var context: Context)
: RecyclerView.Adapter<AdapterAccount.CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterAccount.CustomViewHolder {
        return CustomViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_layout_account, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return accountList?.size!!
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.textViewAccount?.text = accountList!![position].accountName
        holder.container?.setOnClickListener {
            Toast.makeText(context, "Nombre: ${accountList!![position].accountName}", Toast.LENGTH_SHORT).show()
        }
    }

    class CustomViewHolder(itemVw : View): RecyclerView.ViewHolder(itemVw){
        val textViewAccount = itemVw.findViewById<TextView>(R.id.text_view_item_account)
        val container = itemVw.findViewById<ConstraintLayout>(R.id.container_layout_account)
    }
}