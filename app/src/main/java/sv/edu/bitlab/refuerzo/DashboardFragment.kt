package sv.edu.bitlab.refuerzo

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class DashboardFragment : Fragment() {

    private var listener: OnFragmentInteractionListener? = null

    private var collections: MutableList<Account>? = null
    private var list: List<Account>? = null

    private var fragmentView: View? = null
    private var collectionView: RecyclerView? = null

    private var adapterCollection: AdapterAccount? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_dashboard, container, false)

        collectionView = fragmentView?.findViewById(R.id.recycler_view_accounts)

        return fragmentView
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        collections = mutableListOf()

        resources.getStringArray(R.array.accounts).forEach { data ->
            collections?.add(Account(data))
        }




        Log.i("RESPONSE","Size: ${collections?.size}")

        adapterCollection = AdapterAccount(collections, requireContext())

        collectionView?.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(requireContext())
            adapter = adapterCollection
        }

    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    companion object {
        fun invoke() = DashboardFragment()
    }
}
