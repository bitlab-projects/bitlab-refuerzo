package sv.edu.bitlab.refuerzo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment

class MainActivity: AppCompatActivity(), OnFragmentInteractionListener{

    private var listener: OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listener = this

        initializeComponents()
    }

    override fun onFragmentInteraction(index: FragmentsIndex) {

        val transaction = supportFragmentManager.beginTransaction()
        var fragment: Fragment? = null

        when (index) {
            FragmentsIndex.KEY_FRAGMENT_DASHBOARD -> {
                fragment = DashboardFragment.invoke()
                transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out,
                    android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            }
            FragmentsIndex.KEY_FRAGMENT_SETTINGS -> {
                fragment = SettingsFragment.newInstance()
                transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right,
                    android.R.anim.fade_in, android.R.anim.fade_out)
            }
            else -> {
                fragment = DashboardFragment.invoke()
                transaction.setCustomAnimations(android.R.anim.slide_out_right,
                    android.R.anim.slide_in_left)
            }
        }

        transaction
            .replace(R.id.container_fragments, fragment)
            .commit()

    }

    private fun initializeComponents() {

        supportFragmentManager
            .beginTransaction()
            .add(R.id.container_fragments, DashboardFragment.invoke())
            .commit()

        this.findViewById<LinearLayout>(R.id.container_layout_dashboard)
            .setOnClickListener {
                Toast.makeText(this, "Dashboard!",Toast.LENGTH_SHORT).show()
                listener?.onFragmentInteraction(FragmentsIndex.KEY_FRAGMENT_DASHBOARD)
        }

        this.findViewById<LinearLayout>(R.id.container_layout_settings)
            .setOnClickListener {
                Toast.makeText(this, "Settings!",Toast.LENGTH_SHORT).show()
                listener?.onFragmentInteraction(FragmentsIndex.KEY_FRAGMENT_SETTINGS)
        }

    }



}
