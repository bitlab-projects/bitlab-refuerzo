package sv.edu.bitlab.refuerzo

interface OnFragmentInteractionListener {
    fun onFragmentInteraction(index: FragmentsIndex)
}